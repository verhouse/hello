var FB_PERMISSIONS = ['email', 'user_friends'];

Accounts.ui.config({
  requestPermissions: {
    facebook: FB_PERMISSIONS
  }
  // passwordSignupFields: 'USERNAME_ONLY'
});

window.fbAsyncInit = function(){
	FB.init({
	  appId      : '1004722852889730',
	  xfbml      : true,
	  status	 : true,
	  version    : 'v2.1'
	});
};

Template.hello.helpers({
  
});

Template.hello.events({
  
});

var userAddOauthCredentials = function(response, service) {
    var data, oldUser, selector, updateSelector, emailExist;
    data = response;
    selector = "services." + service + ".id";
    updateSelector = "services." + service;
    emailExist = false;

    oldUser = Meteor.users.findOne({
        selector: data.id
    });
    if (oldUser != null) {
        throw new Meteor.Error('access-denied', ("This " + service + " account has already") + "been assigned to another user.");
    }
    _.each(Meteor.user().emails, function(email){
    	emailExist = email.address == data.email ? true : emailExist;
    })
    if(!emailExist){
    	Meteor.users.update({ _id: Meteor.userId() }, {
            $push: {
                "emails": {
                    address: data.email,
                    verified: true,
                    service: service
                }
            }
        });
    }
    return Meteor.users.update({ _id: Meteor.userId() }, {
        $set: {
            updateSelector: data
        }
    });
}

var addUserService = function(service) {
	if (!service) {
		return false;
	}
    switch (service) {
        case "facebook":
        	FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {
			  	FB.api('/me', function(response) {
			  		userAddOauthCredentials(response, service);
			  	}, {scope: FB_PERMISSIONS});
			  }
			  else {
			    FB.login(function(response) {
			   		userAddOauthCredentials(response, service);
			 	}, {scope: FB_PERMISSIONS});
			  }
			});
        // case "google":
        //     return Google.requestCredential({
        //         requestPermissions: ["email", "https://www.googleapis.com/auth/calendar"],
        //         requestOfflineToken: true
        //     }, function(token) {
        //         return Meteor.call("userAddOauthCredentials", token, Meteor.userId(), service, function(err, resp) {
        //             if (err != null) {
        //                 return Meteor.userError.throwError(err.reason);
        //             }
        //         });
        //     });
    }
};

Template.username.events({
  'submit .username-form': function(event){
  	event.preventDefault();
  	var username = $(event.target).find('[type=text]').val();
    if (!username){
    	console.log("Can't set empty username");
        return;
    }
	var update = Meteor.users.update({ _id: Meteor.userId() }, { $set: { "username": username } });
	console.log(update);
	return update;
  }
});

Template.accounts.helpers({
	profiles: function() {
    	return Profiles.find();
	},
});

Template.accounts.events({
	'click .add-service-btn': function (e) {
		e.preventDefault();
		var service = $(event.target).data("service");
		addUserService(service);
	}
});

  
