Meteor.startup(function () {
  if (Profiles.find().count() === 0) {
    var data = [
      {name: "Facebook", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Google", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Twitter", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Instagram", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Tumblr", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "LinkedIn", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "VKontakte", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Skype", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Whatsapp", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Phone Number", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Home Address", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Work Address", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Website", description: "For all the snowboard lovers living in the area", link: ""},
      {name: "Relationship", description: "For all the snowboard lovers living in the area", link: ""}
    ];

    var timestamp = (new Date()).getTime();
    _.each(data, function(profile) {
      var profile_id = Profiles.insert({name: profile.name, description: profile.description, link: profile.link, createdAt: new Date(timestamp)});
      timestamp += 1; // ensure unique timestamp.
    });
  }
});