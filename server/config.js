// FB APP ID: 1004722852889730
// FB APP SECRET: 4d404371a6394b481e4a3ee186cc3a33

Meteor.users.allow({
	update: function(userId, document, fieldNames, modifier) {
		if(userId === Meteor.userId()){
			return true;
		}else{
			throw new Meteor.Error("access-denied", "You can only update your own user");
		}
	}
});

Accounts.onCreateUser(function(options, user){
	if (typeof(user.services.facebook) != "undefined" && options.profile) {
        options.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
        user.profile = options.profile;
        user.emails = [];
        user.emails.push({address: user.services.facebook.email, verified: true})
    }
    return user;
});

Meteor.methods({
	
});